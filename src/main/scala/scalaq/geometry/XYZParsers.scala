package scalaq.geometry

import scalaq.util.parsing.LineOrientedParsers
import scalaq.chemistry.{Atom, Element, Molecule}
import scalaq.linear.Vector

class XYZParsers extends LineOrientedParsers {


    def atom = element ~ coordinate  ^^ {
        case e ~ coor => new Atom(e, coor)
    }

    def element = """\w+""".r ^^ { case e =>  Element(e).get }

    def coordinate = doubleValue ~ doubleValue ~ doubleValue ^^ {
        case x ~ y ~ z => Vector(x, y, z)
    }

    def doubleValue = """[\+\-]?\d+\.\d+""".r ^^ (_.toDouble)

    def intValue = """\d+""".r <~ newLine ^^ (_.toInt)

    def newLine = """\n""".r

    def comment = """.*""".r <~ newLine

    def atoms = repsep(atom, newLine)

    def all = intValue ~ comment ~ atoms ^^ {
        case n ~ c ~ a if n == a.size => Molecule(c, a)
        case _ => throw new RuntimeException("number of atoms and actual atoms are not equal")
    }


}
