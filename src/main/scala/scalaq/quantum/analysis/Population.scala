package scalaq.quantum.analysis

import scalaq.linear.{Matrix, Vector}
import scalaq.chemistry.Molecule
import scalaq.quantum.{BasisSet}

object Population {

    def mullikenCharges(molecule: Molecule, basisSet: BasisSet, p: Matrix, s: Matrix): List[Double] = {
        charges(molecule, basisSet, mulliken(p, s))

    }

    def lowdinCharges(molecule: Molecule, basisSet: BasisSet, p: Matrix, s: Matrix): List[Double] = {
        charges(molecule, basisSet, lowdin(p, s))
    }

    def charges(molecule: Molecule, basisSet: BasisSet, orbitalElectrons: Array[Double]): List[Double] = {
        molecule.atoms.map { atom =>
            val e = for ( i <- basisSet.indices if basisSet.centerOf(i) == atom.position)
                yield orbitalElectrons(i)

            atom.protons + e.reduceLeft(_+_)
        }
    }

    private def mulliken(p: Matrix, s: Matrix): Array[Double] = {
        (p * s).diagonalize
    }

    private def lowdin(p: Matrix, s: Matrix): Array[Double] = {
        val (vec, sd) = Matrix.eigenValueDecomposition(s)
        val sqrtS = Matrix.fillDiagonal(sd.map (math.sqrt _))

        (sqrtS * p * sqrtS).diagonalize
    }
}
