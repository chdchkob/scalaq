package scalaq.quantum.hf

import scalaq.chemistry.{Molecule, Atom}
import scalaq.linear.Matrix
import scalaq.quantum.{BasisSet}
import java.io.PrintWriter
import java.io.Writer



/**
 * Hartree Fock method
 */
abstract class HartreeFock(val charges: Int, val multiplicity: Int, val basisSet: BasisSet, val molecule: Molecule) {

    require(molecule.atoms.size > 0)
    require(basisSet.size > 0)
    require(multiplicity >= 0)

    /**
     *
     */
    def this(charges: Int, multiplicity: Int, basisName: String, molecule: Molecule) =
        this(charges, multiplicity, BasisSet.load(basisName, molecule), molecule)

    /**
     *
     */
    def startWithWriter(writer: PrintWriter): Unit =
        start(HartreeFock.writeStatus(writer))


    /**
     *
     */
    def start(statusCallBack: HartreeFock.Status => Unit): Unit = start(100, 1.0E-8)(statusCallBack)


    /**
     * Self Consistent Field calculation for Hartree Fock Method
     */
    def start(maxLoop: Int, epsilon: Double)(statusCallBack: HartreeFock.Status => Unit): Unit


    /**
     * Total nuclear-nuclear interaction energy
     */
    def nuclearNuclearEnergy: Double = {
        val enPairs = for {
            a <- molecule.atoms
            b <- molecule.atoms if b != a
        } yield a.protons * b.protons / a.position.distance(b.position)

        enPairs.reduceLeft(_+_)
    }


    /**
     * Kinetic energy matrix
     */
    def tMatrix: Matrix = Matrix.fillSymmetric(basisSet.size) { (i, j) => basisSet.kinetic(i,j) }


    /**
     * Potential energy matrix
     */
    def vMatrix: Matrix = Matrix.fillSymmetric(basisSet.size) {
        (i,j) => molecule.atoms.foldLeft(0.0) {
            (acc, atom) => acc + basisSet.nuclear(atom, i, j)
        }
    }

    /**
     * Overlap matrix
     */
    lazy val overlapMatrix: Matrix = Matrix.fillSymmetric(basisSet.size)(basisSet.overlap _)


    /**
     * Ortho-normalize overlap maptrix
     */
    lazy val xMatrix: Matrix = Matrix.symmetricOrthonormalize(overlapMatrix)


    /**
     * Single-electron Harmiltonian matrix
     */
    lazy val hMatrix: Matrix = Matrix.fillSymmetric(basisSet.size) {
        (i, j) =>
            val ke = basisSet.kinetic(i, j)
            val vne = molecule.atoms.foldLeft(0.0) {
                (acc, atom) => acc + basisSet.nuclear(atom, i, j)
            }

            ke + vne
    }

}

/**
 *
 */
object HartreeFock {

    val logger = org.slf4j.LoggerFactory.getLogger(this.getClass)

    /**
     *
     */
    def restrictedClosedShell(basisName: String, molecule: Molecule): HartreeFock =
        restrictedClosedShell(0, basisName, molecule)

    /**
     *
     */
    def restrictedClosedShell(charges: Int, basisName: String,
                              molecule: Molecule): HartreeFock =
        new RestrictedHartreeFock(charges, basisName, molecule)

    /**
     *
     */
    def unrestrictedOpenShell(charges: Int, multiplicity: Int,
                              basisName: String, molecule: Molecule): HartreeFock =
        new UnrestrictedHartreeFock(charges, multiplicity, basisName, molecule)

    /**
     * Hartree-Fock Calculation status
     */
    sealed abstract class Status
    case class Start(charges: Int, multiplicity: Int,
                     basisSet: BasisSet, molecule: Molecule,
                     maxLoop: Int, eps: Double) extends Status

    case class StartLoop(n: Int) extends Status

    case class EndLoop(n: Int, etotal: Double) extends Status

    case class Finish(n: Int, etotal: Double, enAlpha: Array[Double], enBeta: Option[Array[Double]],
                      pTotal: Matrix, pSpin: Option[Matrix]) extends Status

    case class ReachMaxLoop(n: Int, enAlpha: Array[Double], enBeta: Option[Array[Double]],
                            pTotal: Matrix, pSpin: Option[Matrix]) extends Status

    /**
     *
     */
    def writeStatus(writer: PrintWriter): (Status => Unit) = {
        case Start(charges, multiplicity, basisSet, molecule, maxLoop, eps) =>
            writer println ("Start with max loop: %d and epsilon: %f" format (maxLoop, eps))
            writer println ("BasisSet: %s" format (basisSet toString))
            writer println ("Charges: %d" format charges)
            writer println ("Multiplicity: %d" format multiplicity)
            writer println ("Molecule:\n %s" format  (molecule toString))
        case StartLoop(n) =>
            writer println (line)
            writer println ("Iterate: %d" format n)
        case EndLoop(n, etotal) =>
            writer println("  Energy: %15.9f" format etotal)

        case Finish(n, etotal, enAlpha, enBeta, pAlpha, pBeta) =>
            writer println (line)
            writer println ("SCF Done: E = %15.9f A.U. after %d cycles" format (etotal, n))
            writer println ("Normal termination")

        case _: ReachMaxLoop =>
            writer println ("Reach max loop")
    }

    private val line = "----------------------------------"

    def electronicEnergy(p: Matrix, h: Matrix, f: Matrix): Double = {
        // Eq. 3.274 [Szabo]
        val es = for (i <- 0 until p.nrows; j <- 0 until p.ncols)
                      yield p(j,i) * (h(i,j) + f(i,j))

        // electronic energy for each iteration
        0.5 * es.reduceLeft(_+_)
    }
}
