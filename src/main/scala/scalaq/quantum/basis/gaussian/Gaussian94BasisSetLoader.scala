package scalaq.quantum.basis.gaussian

import scalaq.chemistry.{Element,Atom,Molecule}
import scalaq.quantum.{BasisSetLoader, BasisSet}
import scala.io.Source


class Gaussian94BasisSetLoader extends BasisSetLoader {
    type PGBFData = (Int, Int, Int, Double)
    type CGBFData = List[(Double,PGBFData)]

    def toURL(basisName: String) = {
        val fileName = "/basis/gaussian94/%s.bas".format(basisName)
        getClass.getResource(fileName)
    }

    def isDefinedAt(basisName: String): Boolean = {
        toURL(basisName) != null
    }

    def load(basisName: String, molecule: Molecule): BasisSet = {
        val src = Source.fromURL(toURL(basisName))
        val sep = "****"
        val comment = "!"
        val whitespace = """\s+"""

        val elementNames = molecule.atoms.map { _.element.toString }

        val basisBlocks = src.getLines.
            // remove pre- and post-whitespaces for each line
            map(_.trim).
            // remove all line before `sep`
            dropWhile(_ != sep).
            // remove empty lines and comment lines
            filter(x => !x.isEmpty && !x.startsWith(comment)).
            // split input in to groups of element data
            foldLeft(List[List[String]]()) {
                (blocks, line) => line match {
                    case "****" => List[String]()::blocks
                    case _ => (line::blocks.head)::blocks.tail
                }
            }.
            // remove empty groups
            filter(_ != Nil).
            // keep only element data that match elements in molecule
            filter(l => elementNames.exists(_ == l.last.split(whitespace)(0))).
            // reverse each element data into natural order
            map(_.reverse)

        val basisData: List[(String, List[(Double,List[CGBFData])])] = basisBlocks.map {
            block =>
                val elementName = block.head.split(whitespace)(0)
                val (orbitals, params) = block.tail.partition(_.split(whitespace)(0).matches("[SPDFGH]+"))

                (elementName, pgfdata(orbitals, params, Nil))
        }


        val cgbfs = molecule.atoms.map { atom =>
            basisData.find(x => x._1 == atom.element.toString).map { case (elementName, ncgbfs) =>
                ncgbfs.map { case (norm, cgbfs) =>
                    cgbfs.map {
                        cgbf =>
                            val temp = cgbf.map {
                                case (coeff, (l,m,n,alpha)) => (coeff * norm, new PGBF(l, m, n, atom.position, alpha)) 
                            }
                        new CGBF(temp)
                    }
                }.flatMap(x => x)
            }.get
        }


        new ContractedGaussianBasisSet(basisName, cgbfs.flatMap(x => x))

    }

    def pgfdata(orbs: List[String], params: List[String], result: List[(Double, List[CGBFData])]): List[(Double, List[CGBFData])] = {
        val whitespace = """\s+"""
        if (orbs == Nil) {
            result
        } else {
            val field = orbs.head.split(whitespace)
            val orb = field(0)
            val n = field(1).toInt
            val norm = field(2).toDouble
            val (cparams, nparams) = params.splitAt(n)

            val pgfparams: List[CGBFData] = orb match {
                case "S" => cparams.map { line =>
                        val lineArray = line.split(whitespace)
                        val alpha = lineArray(0)
                        val coeff = lineArray(1)

                        (coeff.toDouble, (0, 0, 0, alpha.toDouble))
                    } :: Nil
                case "P" => 
                    val tuple = cparams.foldLeft((List[(Double, PGBFData)](),List[(Double,PGBFData)](), List[(Double,PGBFData)]())) { (t, line) =>
                        val lineArray = line.split(whitespace)
                        val alpha = lineArray(0)
                        val coeff = lineArray(1)

                        ((coeff.toDouble, (1, 0, 0, alpha.toDouble)) :: t._1,
                         (coeff.toDouble, (0, 1, 0, alpha.toDouble)) :: t._2,
                         (coeff.toDouble, (0, 0, 1, alpha.toDouble)) :: t._3)
                    }
                    tuple._1 :: tuple._2 :: tuple._3 :: Nil

                case "SP" =>
                    val tuple = cparams.foldLeft((List[(Double, PGBFData)](),List[(Double,PGBFData)](), List[(Double,PGBFData)](),List[(Double,PGBFData)]())){ (t, line) =>
                        val lineArray = line.split(whitespace)
                        val alpha = lineArray(0)
                        val coeff1 = lineArray(1)
                        val coeff2 = lineArray(2)

                        ((coeff1.toDouble, (0, 0, 0, alpha.toDouble)) :: t._1,
                         (coeff2.toDouble, (1, 0, 0, alpha.toDouble)) :: t._2,
                         (coeff2.toDouble, (0, 1, 0, alpha.toDouble)) :: t._3,
                         (coeff2.toDouble, (0, 0, 1, alpha.toDouble)) :: t._4)
                    }

                    tuple._1 :: tuple._2 :: tuple._3 :: tuple._4 :: Nil

                case "D" =>
                    val tuple = cparams.foldLeft((List[(Double,PGBFData)](),
                                                  List[(Double,PGBFData)](),
                                                  List[(Double,PGBFData)](),
                                                  List[(Double,PGBFData)](),
                                                  List[(Double,PGBFData)](),
                                                  List[(Double,PGBFData)]())) { (t, line) =>
                        val lineArray = line.split(whitespace)
                        val alpha = lineArray(0)
                        val coeff = lineArray(1)

                        ((coeff.toDouble, (2, 0, 0, alpha.toDouble)) :: t._1,
                         (coeff.toDouble, (0, 2, 0, alpha.toDouble)) :: t._2,
                         (coeff.toDouble, (0, 0, 2, alpha.toDouble)) :: t._3,
                         (coeff.toDouble, (1, 1, 0, alpha.toDouble)) :: t._4,
                         (coeff.toDouble, (0, 1, 1, alpha.toDouble)) :: t._5,
                         (coeff.toDouble, (1, 0, 1, alpha.toDouble)) :: t._6)
                    }
                    tuple._1 :: tuple._2 :: tuple._3 ::
                    tuple._4 :: tuple._5 :: tuple._6 :: Nil

                case other => throw new RuntimeException(other + "Not support")
            }

            val newresult = (norm, pgfparams)::result

            pgfdata(orbs.tail, nparams, newresult)
        }
    }

}
