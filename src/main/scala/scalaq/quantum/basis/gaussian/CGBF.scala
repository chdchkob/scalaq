package scalaq.quantum.basis.gaussian

import scalaq.linear.Vector
import scala.math.sqrt

/**
 * Contracted gaussian basis function (CGBF)
 */
class CGBF(val pgbfs: List[(Double, PGBF)]) {

    require(pgbfs.size > 0)

    require {
        pgbfs.tail.forall {
            case (_,pgbf) => pgbf.center == center
        }
    }

    require {
        val l = pgbfs.head._2.l
        val m = pgbfs.head._2.m
        val n = pgbfs.head._2.n

        pgbfs.tail.forall {
            case (_,pgbf) => pgbf.l == l && pgbf.m == m && pgbf.n == n
        }
    }

    lazy val norm: Double = 1/sqrt(overlap(this))

    lazy val center: Vector = pgbfs.head._2.center


    def apply(position: Vector): Double = {
        pgbfs.map {
            case (coeff, pgbf) => coeff * pgbf(position)
        }.reduceLeft(_+_)
    }

    override def toString: String = {
        (
          "Norm: %12.6f".format(norm) ::
            pgbfs.map {
                pgbf => "%12.6f    %s".format(pgbf._1, pgbf._2)
            }
        ).mkString("\n")
    }

    def overlap(other: CGBF): Double = {
        var sum = 0.0
        for ((ci, fi) <- this.pgbfs; (cj, fj) <- other.pgbfs) {
            sum += ci * cj * PGBF.overlap(fi, fj)
        }
        sum
    }

    def kinetic(other: CGBF): Double = {
        var sum = 0.0
        for ((ci, fi) <- this.pgbfs; (cj, fj) <- other.pgbfs) 
            sum += ci * cj * PGBF.kinetic(fi, fj)
        sum
    }

    def nuclear(other: CGBF, nuclearCenter: Vector): Double = {
        var sum = 0.0
        for ((ci, fi) <- this.pgbfs; (cj, fj) <- other.pgbfs) {
            sum += ci * cj * PGBF.nuclear(fi, fj, nuclearCenter)
        }

        sum
    }

    def coulomb(a: CGBF, b: CGBF, c: CGBF): Double = {
        var sum = 0.0
        for ((ci, fi) <- pgbfs; (cj, fj) <- a.pgbfs; (ck, fk) <- b.pgbfs; (cl, fl) <- c.pgbfs) {
            sum += ci * cj * ck * cl * PGBF.coulomb(fi, fj, fk, fl)
        }

        sum
     }
}
