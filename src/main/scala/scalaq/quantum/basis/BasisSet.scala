package scalaq.quantum

import scalaq.chemistry.{Atom, Molecule}
import scalaq.linear.{Matrix, Vector}

trait BasisSet {
    def name: String

    def size: Int

    def indices = 0 until size

    def chargeDensity(p: Matrix, r: Vector): Double = {
        val ch = for ( i <- indices; j <- indices) yield p(i,j)*prob(i, r)*prob(j, r)

        ch.reduceLeft(_+_)
    }

    def prob(i: Int, r: Vector): Double

    def centerOf(i: Int): Vector

    def overlap(i: Int, j: Int): Double

    def kinetic(i: Int, j: Int): Double

    def nuclear(atom: Atom, i: Int, j: Int): Double

    def coulomb(i: Int, j: Int, k: Int, l: Int): Double
}

object BasisSet {

    private val logger = org.slf4j.LoggerFactory.getLogger(this.getClass)

    private lazy val basisSetLoaders = {
        import scala.collection.JavaConversions._
        val list = java.util.ServiceLoader.load(classOf[BasisSetLoader]).toList
        require(list.size > 0)
        logger.info("Using {} for BasisSetLoader", list)
        list
    }


    def load(basisName: String, molecule: Molecule): BasisSet = {

        val basisSetLoader = basisSetLoaders.find(_.isDefinedAt(basisName))

        basisSetLoader match {
            case Some(loader) => loader.load(basisName, molecule)
            case _ => throw new IllegalArgumentException("Not found basis set %s for %s.".format(basisName, molecule.toString))
        }
    }
}
