package scalaq.quantum

import scalaq.chemistry.Molecule

/**
 *
 */
trait BasisSetLoader {

    def isDefinedAt(id: String): Boolean

    def load(id: String, molecule: Molecule): BasisSet
}
