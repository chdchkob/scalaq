package scalaq.linear

case class Vector(val x: Double, val y: Double, val z: Double) {

    def distance2(other: Vector): Double = {
        val dx = x - other.x
        val dy = y - other.y
        val dz = z - other.z

        dx * dx + dy * dy + dz * dz
    }

    def distance(other: Vector): Double = math.sqrt(distance2(other))

    def normalize: Vector = (this / distance(Vector.origin))

    def *(value: Double): Vector = Vector(x * value,  y * value, z * value)

    def /(value: Double): Vector = Vector(x / value,  y / value, z / value)

    def +(other: Vector): Vector = Vector(x + other.x, y + other.y, z + other.z)

    def -(other: Vector): Vector = Vector(x - other.x, y - other.y, z - other.z)

    def dot(other: Vector): Double = {
        x * other.x + y * other.y + z * other.z
    }

    def cross(other: Vector): Vector = Vector(
        y * other.z - z * other.y,
        z * other.x - x * other.z,
        x * other.y - y * other.x
    )

    def unary_- : Vector = Vector(-x, -y, -z)

    override def toString: String = "[%7.4f  %7.4f  %7.4f]".format(x, y, z)
}


object Vector {
    val origin = Vector(0, 0, 0)

    def normalVector(a: Vector, b: Vector, c: Vector): Vector = {
        val ab = a - b
        val cb = c - b

        (cb cross ab).normalize
    }

    def rotate(target: Vector, normal: Vector, alpha: Double): Vector = {
        val t = normal * (normal dot target)
        val n = target - t
        val c = normal cross target

        n * math.cos(alpha) + c * math.sin(alpha) + t
    }
}
