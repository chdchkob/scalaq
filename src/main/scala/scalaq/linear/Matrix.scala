package scalaq.linear

trait Matrix {

    def size: (Int, Int)

    def nrows = size._1

    def ncols = size._2

    def apply(i: Int, j: Int): Double

    def update(i: Int, j: Int, value: Double): Unit

    def -(m: Matrix): Matrix

    def +(m: Matrix): Matrix

    def *(m: Matrix): Matrix

    def *(n: Double): Matrix

    def /(n: Double): Matrix

    def transpose: Matrix

    def trace: Double = diagonalize.reduceLeft(_+_)

    def subMatrix(startRow: Int, endRow: Int, startColumn: Int, endColumn: Int): Matrix

    def takeColumns(n: Int) = subMatrix(0, nrows - 1, 0, n - 1)

    def diagonalize: Array[Double] = {
        (for (i <- 0 until nrows if i < ncols) yield apply(i,i)).toArray
    }

    def isSquare: Boolean = (nrows == ncols)

    def isSymmetry: Boolean = {
        var compares = for (i <- 0 until nrows; j <- 0 until i) yield apply(i,j) == apply(j,i)
        compares.forall(true == _)
    }

    def forceSymmetric: Matrix = {
        require(nrows == ncols)
        Matrix.fillSymmetric(nrows) { (i,j) => apply(i,j) }
    }

    def elements = for (i <- 0 until ncols; j <- 0 until nrows)  yield apply(i,j)

    override def toString: String = {
        val sb = new StringBuilder
        sb.append("Dimension: %dx%d\n" format (nrows, ncols))
        for (i <- 0 until nrows) {
            for (j <- 0 until ncols) {
                sb.append("%13.8f " format (apply(i,j)))
            }
            sb.append("\n")
        }

        sb.toString
    }
}

object Matrix {

    val Zero = new Array[Double](1)

    def zeroes(i: Int, j: Int): Matrix = LinearAlgebra.zeroes(i, j)

    def zeroes(i: Int): Matrix = zeroes(i, i)

    def sum(m: Matrix): Double = {
        (for (i <- 0 until m.nrows; j <- 0 until m.ncols) yield m(i,j)).foldLeft(0.0)(_+_)
    }

    def fillDiagonal(diag: Array[Double]): Matrix = {
        fillSymmetric(diag.size) { (i, j) => if (i == j) diag(i) else 0.0 }
    }

    def eigenValueDecomposition(m: Matrix): (Matrix, Array[Double]) = LinearAlgebra.eigenValueDecomposition(m)

    def symmetricOrthonormalize(m: Matrix): Matrix = {
        require(m.isSymmetry)

        val (eigenVector, eigenValue) = eigenValueDecomposition(m)
        val data = eigenValue.map(i => 1 / math.sqrt(i))
        val transformEigenValue = Matrix.fillDiagonal(data)

        eigenVector * transformEigenValue * eigenVector.transpose
    }

    def canonicalOrthonormalize(m: Matrix): Matrix = {
        require(m.isSymmetry)

        val (eigenVector, eigenValue) = eigenValueDecomposition(m)
        val data = eigenValue.map(i => 1 / math.sqrt(i))

        eigenVector *  Matrix.fillDiagonal(data)
    }

    def fillSymmetric(s: Int)(f: (Int, Int) => Double): Matrix = {
        val m = zeroes(s)

        for (i <- 0 until s; j <- 0 to i) {
            m(i,j) = f(i,j)
            m(j,i) = m(i,j)
        }

        m
    }
}
