package scalaq.linear

trait LinearAlgebra {

    def zeroes(i: Int, j: Int): Matrix

    def eigenValueDecomposition(m: Matrix): (Matrix, Array[Double])
}

object LinearAlgebra {

    val instance = java.util.ServiceLoader.load(classOf[LinearAlgebra]).iterator.next

    def zeroes(i: Int, j: Int): Matrix = instance.zeroes(i,j)

    def eigenValueDecomposition(m: Matrix): (Matrix, Array[Double]) = instance.eigenValueDecomposition(m)
}
