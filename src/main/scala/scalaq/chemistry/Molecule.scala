package scalaq.chemistry

class Molecule(val label: String, val atoms: List[Atom]) {
    require(atoms.size > 0)
    require(atoms.map(_.position).distinct.size == atoms.size)

    def protons = atoms.foldLeft(0)(_+_.protons)

    def electrons(charges: Int) = protons - charges

    override def toString: String = {
        atoms.map(_.toString).mkString("\n")
    }
}


object Molecule {
    import scalaq.linear.Vector

    def apply(atoms: Atom*): Molecule = apply(atoms.toList)

    def apply(label: String, atoms: Atom*): Molecule = apply(label, atoms.toList)

    def apply(atomList: List[Atom]): Molecule = new Molecule("", atomList)

    def apply(label: String, atomList: List[Atom]): Molecule = new Molecule(label, atomList)

    class ZMatrixBuilder {
        val yAxis = Vector(0, 1, 0)

        var atoms: List[Atom] = Nil

        def add(e: Element): Atom = {
                atoms ::= Atom(e)

                atoms.head
        }

        def add(e: Element, bond: (Atom, Double)): Atom = {
            val pos = Vector(0, 0, bond._2)
            atoms ::= new Atom(e, bond._1.position + pos) // add on z axis
            atoms.head
        }

        def add(e: Element, bond: (Atom, Double), angle: (Atom, Double)): Atom = {
            val a = bond._1.position
            val b = angle._1.position
            val length = bond._2
            val alpha = angle._2

            val ab = (b - a).normalize

            val pos = Vector.rotate(ab, yAxis, alpha)
            atoms ::= new Atom(e, pos * length + a)
            atoms.head
        }

        def add(e: Element, bond: (Atom, Double), angle: (Atom, Double), torsion: (Atom, Double)): Atom =  {
            val b = bond._1.position
            val length = bond._2

            val c = angle._1.position
            val alpha = angle._2

            val d = torsion._1.position
            val dihedral = torsion._2

            val bc = (c - b).normalize
            val p = Vector.normalVector(b, c, d)
            val pos2 = Vector.rotate(bc, p, alpha)

            val pos = Vector.rotate(pos2, bc, -dihedral)
            atoms ::= new Atom(e, pos * length + b)
            atoms.head
        }
    }
}
