package scalaq.chemistry


object AtomicUnit {
    class Length(val value: Double) {
        def A: Double = value / 0.52918
        def toA: Double = value * 0.52918
    }

    class Energy(val value: Double) {
        def J: Double = value / 4.3598E-18
        def toJ: Double = value * 4.3598E-18
    }

    class Mass(val value: Double) {
        def kg: Double = value / 9.1095E-31
        def toKg: Double = value * 9.1095E-31
    }
    class Charge(val value: Double) {
        def C: Double = value / 1.6022E-19
        def toC: Double = value * 1.6022E-19
    }

    implicit def doubleToLength(value: Double): Length = new Length(value)
    implicit def doubleToEnergy(value: Double): Energy = new Energy(value)
    implicit def doubleToMass(value: Double): Mass = new Mass(value)
    implicit def doubleToCharge(value: Double): Charge = new Charge(value)
}
