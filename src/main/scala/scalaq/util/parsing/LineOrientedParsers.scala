package scalaq.util.parsing

import scala.util.parsing.combinator._

class LineOrientedParsers extends RegexParsers {

    override val whiteSpace = """[\t\x0B\f\r ]*""".r
}
