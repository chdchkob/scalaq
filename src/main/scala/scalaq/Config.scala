package scalaq

import scala.io.Source
import chemistry.{Element,Molecule,Atom}

case class Config(molecule: Molecule, charge: Int, multiplicity: Int, method: String, basisName: String)
object Config {

  /**
   * Parse enhanced XYZ format.
   * The original comment line is used to be an computational configuration parameter line.
   * @param in
   * @return
   */
    def parse(in: Source): Config = {
        val lines = in.getLines
        val numberOfAtoms = lines.next.trim.toInt
        val commands = lines.next.split("/").map(_.trim)
        val method = commands(0)
        val basisName = commands(1)
        val charge = commands(2).toInt
        val multiplicity = commands(3).toInt

        val atoms = for (atom <- lines) yield {
            val atomFields = atom.split("""\s+""")
            val element = Element(atomFields(0)).get
            val x = atomFields(1).toDouble
            val y = atomFields(2).toDouble
            val z = atomFields(3).toDouble
            Atom(element, x, y, z)
        }


        Config(Molecule(atoms.toList), charge, multiplicity, method, basisName)
    }
}
