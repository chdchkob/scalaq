package scalaq.chemistry

import Element.H
import scalaq.linear.Vector
import org.scalatest.FunSuite

class AtomSuite extends FunSuite {
    val hAtom = Atom(H)

    val z1 = Vector(0.0, 0.0, 1.0)

    val z2 = Vector(0.0, 0.0, 2.0)

    test("create atom") {
        assert(Vector(0.0, 0.0, 0.0) === hAtom.position)

        val atom = new Atom(H, z1)

        assert(Element.H === atom.element)
        assert(z1 === atom.position)
    }

    test("move to a position") {
        assert(z1 === (hAtom moveTo z1).position)
    }

    test("move by a fraction") {
        assert(z2 === (hAtom  move z1 move z1).position)
    }

    test("atomic number and protons") {
        assert(Element.H.atomicNumber === hAtom.protons)
    }
}
