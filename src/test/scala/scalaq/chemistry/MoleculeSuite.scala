package scalaq.chemistry

import Element.{H, O}
import scalaq.linear.Vector
import org.scalatest.FunSuite
import org.scalatest.matchers.ShouldMatchers

class MoleculeSuite extends FunSuite with ShouldMatchers {
    test("Hydrogen molecule") {
        val h2 = Molecule(Atom(H), Atom(H).move(Vector(0, 0, 1)))
        val charges = 0

        assert(2 === h2.protons)
        assert(2 === h2.electrons(charges))
    }

    test("Z-matrix") {

        val zmat = new Molecule.ZMatrixBuilder

        val h1 = zmat add (Element.H)
        h1.element should be (H)
        h1.position.x should be (0.0 plusOrMinus 1E-11)
        h1.position.y should be (0.0 plusOrMinus 1E-11)
        h1.position.z should be (0.0 plusOrMinus 1E-11)

        val h2 = zmat add (Element.H, h1 -> 1)
        h2.element should be (H)
        h2.position.x should be (0.0 plusOrMinus 1E-11)
        h2.position.y should be (0.0 plusOrMinus 1E-11)
        h2.position.z should be (1.0 plusOrMinus 1E-11)

        val h3 = zmat add (Element.H, h2 -> 2, h1 -> - math.Pi/2)
        h3.element should be (H)
        h3.position.x should be (2.0 plusOrMinus 1E-11)
        h3.position.y should be (0.0 plusOrMinus 1E-11)
        h3.position.z should be (1.0 plusOrMinus 1E-11)

        val h4 = zmat add (Element.H, h1 -> 1, h2 -> math.Pi/2, h3 -> 0)
        h4.element should be (H)
        h4.position.x should be (1.0 plusOrMinus 1E-11)
        h4.position.y should be (0.0 plusOrMinus 1E-11)
        h4.position.z should be (0.0 plusOrMinus 1E-11)

        val h5 = zmat add (Element.H, h1 -> 1, h2 -> math.Pi/2, h3 -> math.Pi/2)
        h5.element should be (H)
        h5.position.x should be (0.0 plusOrMinus 1E-11)
        h5.position.y should be (-1.0 plusOrMinus 1E-11)
        h5.position.z should be (0.0 plusOrMinus 1E-11)

        val h6 = zmat add (Element.H, h1 -> 1, h2 -> math.Pi/2, h3 -> math.Pi)
        h6.element should be (H)
        h6.position.x should be (-1.0 plusOrMinus 1E-11)
        h6.position.y should be (0.0 plusOrMinus 1E-11)
        h6.position.z should be (0.0 plusOrMinus 1E-11)

        val h7 = zmat add (Element.H, h1 -> 1, h2 -> math.Pi/2, h3 -> - math.Pi/2)
        h7.element should be (H)
        h7.position.x should be (0.0 plusOrMinus 1E-11)
        h7.position.y should be (1.0 plusOrMinus 1E-11)
        h7.position.z should be (0.0 plusOrMinus 1E-11)
    }
}

