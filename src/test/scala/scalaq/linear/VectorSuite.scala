package scalaq.linear

import org.scalatest.{FunSuite, matchers}

class VectorSuite extends FunSuite with matchers.ShouldMatchers {

    test("normalize vector") {
        (Vector(5, 0, 0).normalize) should be (Vector(1, 0, 0))
        (Vector(0, 5, 0).normalize) should be (Vector(0, 1, 0))
        (Vector(0, 0, 5).normalize) should be (Vector(0, 0, 1))
        (Vector(1, 1, 0).normalize) should be (Vector(1/math.sqrt(2.0), 1/math.sqrt(2.0), 0))

    }

    test("unary negate (-)") {
      -Vector(1,1,1) should be (Vector(-1, -1, -1))
    }

    test("cross vector") {
        val x = Vector(1, 0, 0)
        val y = Vector(0, 1, 0)
        val z = Vector(0, 0, 1)
        val xm = Vector(-1, 0, 0)
        val ym = Vector(0, -1, 0)
        val zm = Vector(0, 0, -1)
        (x cross x) should be (Vector.origin)
        (y cross y) should be (Vector.origin)
        (z cross z) should be (Vector.origin)

        (x cross y) should be (z)
        (y cross z) should be (x)
        (z cross x) should be (y)

        (y cross x) should be (zm)
        (z cross y) should be (xm)
        (x cross z) should be (ym)
    }

    test("rotate vector") {
        val x = Vector(5, 5, 0)
        val z = Vector(0, 0, 1)

        val y = Vector.rotate(x, z, math.Pi/2)
        (y.x) should be (-5.0 plusOrMinus 1E-10)
        (y.y) should be ( 5.0 plusOrMinus 1E-10)
        (y.z) should be ( 0.0 plusOrMinus 1E-10)
    }
}
