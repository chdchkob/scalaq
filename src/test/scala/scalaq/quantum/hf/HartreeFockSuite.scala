package scalaq.quantum.hf

import scalaq.linear.Matrix
import scalaq.chemistry.{Atom, Element, Molecule}
import Element.{H, He}
import org.scalatest.FunSuite
import org.scalatest.matchers.ShouldMatchers
import HartreeFock._

class HartreeFockSuite extends FunSuite with ShouldMatchers {

    test("HeH+ with RHF/STO3G") {

        val aHeHp = Molecule(
            Atom(He, 0, 0, 0),
            Atom(H, 0, 0, 1.4632)
        )

        val charges = 1

        val rhf = restrictedClosedShell(charges, "Szabo STO-3G", aHeHp)

        val s = rhf.overlapMatrix
        s(0,0) should be (1.0 plusOrMinus 1E-9)
        s(0,1) should be (0.4507704116 plusOrMinus 1E-9)
        s(1,1) should be (1.0 plusOrMinus 1E-9)

        val h = rhf.hMatrix
        h(0,0) should be (-0.2652744703E+1 plusOrMinus 1E-6)
        h(0,1) should be (-0.1347205024E+1 plusOrMinus 1E-6)
        h(1,1) should be (-0.1731828436E+1 plusOrMinus 1E-6)

        rhf.start {
            case Finish(n, _, e, _, _, _) =>
                e(0) should be (-0.1597448132E+1 plusOrMinus 1E-5)
                e(1) should be (-0.6166851652E-1 plusOrMinus 1E-5)

            case _ => ()
        }

    }

    test("HeH+ with UHF/STO3G") {

        val aHeHp = Molecule(
            Atom(He, 0, 0, 0),
            Atom(H, 0, 0, 1.4632)
        )

        val charges = 1

        val rhf = unrestrictedOpenShell(charges, 1, "Szabo STO-3G", aHeHp)

        val s = rhf.overlapMatrix
        s(0,0) should be (1.0 plusOrMinus 1E-9)
        s(0,1) should be (0.4507704116 plusOrMinus 1E-9)
        s(1,1) should be (1.0 plusOrMinus 1E-9)

        val h = rhf.hMatrix
        h(0,0) should be (-0.2652744703E+1 plusOrMinus 1E-6)
        h(0,1) should be (-0.1347205024E+1 plusOrMinus 1E-6)
        h(1,1) should be (-0.1731828436E+1 plusOrMinus 1E-6)

        rhf.start {
            case Finish(n, _, ea, Some(eb), _, _) =>
                ea(0) should be (-0.1597448132E+1 plusOrMinus 1E-5)
                ea(1) should be (-0.6166851652E-1 plusOrMinus 1E-5)
                eb(0) should be (-0.1597448132E+1 plusOrMinus 1E-5)
                eb(1) should be (-0.6166851652E-1 plusOrMinus 1E-5)

            case _ => ()
        }

    }


    test("H2 with RHF/STO3G") {

        val h2Molecule = Molecule(
            Atom(H, 0.0, 0.0, -0.7),
            Atom(H, 0.0, 0.0, 0.7)
        )

        val rhf = restrictedClosedShell("STO-3G", h2Molecule)

        val s = rhf.overlapMatrix
        s(0,0) should be (1.0 plusOrMinus 1E-6)
        s(0,1) should be (0.6593 plusOrMinus 1E-3)
        s(1,1) should be (1.0 plusOrMinus 1E-6)
    }
}
