package scalaq.quantum.basis.gaussian

import scalaq.linear.Vector
import org.scalatest.FunSuite
import org.scalatest.matchers.ShouldMatchers


class PGBFSuite extends FunSuite with ShouldMatchers {
    val eps = 1E-8
    val origin = Vector(0.0, 0.0, 0.0)

    val p1 = new PGBF(0, 0, 0, Vector(0.0, 0.0, 0.0), 0.5)
    val p2 = new PGBF(1, 0, 0, Vector(0.0, 0.0, 1.0), 1.0)
    val p3 = new PGBF(1, 1, 0, Vector(0.0, 0.0, 1.0), 0.1)
    val p4 = new PGBF(0, 0, 1, Vector(0.0, 1.0, 0.0), 0.2)
    val p5 = new PGBF(0, 0, 0, Vector(0.0, 0.0, 1.0), 0.5)

    test ("primitive normalization") {
         p1.norm should be (0.423777208124 plusOrMinus eps)
         p2.norm should be (1.42541094071 plusOrMinus eps)
         p3.norm should be (0.0506955785342 plusOrMinus eps)
         p4.norm should be (0.190645949441 plusOrMinus eps)
         p5.norm should be (0.423777208124 plusOrMinus eps)
    }

    test ("product center") {
        p1.productCenter(p2) should be (Vector(0.0,0.0,2.0/3.0))
        p2.productCenter(p3) should be (Vector(0.0,0.0,1.0))
    }

    test ("primitive overlap") {
        PGBF.overlap(p1, p1) should be (1.0 plusOrMinus eps)
        PGBF.overlap(p1, p5) should be (0.778800783071 plusOrMinus eps)
    }

    test ("primitive kinetic") {
        PGBF.kinetic(p1, p1) should be (0.75 plusOrMinus eps)
        PGBF.kinetic(p2, p2) should be (2.5 plusOrMinus eps)
    }

// data from PyQuante is failed to achieve, but for original paper is passed
    test ("coulomb repulsion") {
        PGBF.coulomb(p1,p2,p3,p4) should be (0.00285248217211 plusOrMinus eps)
    }

}
