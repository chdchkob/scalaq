package scalaq.quantum.basis.gaussian

import scalaq.linear.Vector
import org.scalatest.FunSuite
import org.scalatest.matchers.ShouldMatchers

class CGBFSuite extends FunSuite with ShouldMatchers {

    val origin = Vector(0.0, 0.0, 0.0)

    val err = 1.0E-6

    // Table (1) from Hiroshi TAKETA, Sigeru HUZINAGA and Kiyosi O-OHATA
    val params  = Map(
        // 4 Gaussian function
        // (alpha, coefficient)
        (0.101410, 0.381889),
        (0.331608, 0.520378),
        (1.27973 , 0.200037),
        (7.23370 , 0.0385012)
    )

    def s4g(center: Vector) =
        for (param <- params) yield (param._2, new PGBF(0,0,0, center, param._1))

    test ("Overlap, kinetic and nuclear integration and compare" +
          "with Table (1) from Hiroshi TAKETA, Sigeru HUZINAGA and Kiyosi O-OHATA") {
        val cgbf1sa = new CGBF(s4g(origin).toList)
        val cgbf1sb = new CGBF(s4g(Vector(0.0, 0.0, 4.0)).toList)
        val cgbf1sc = new CGBF(s4g(Vector(0.0, 0.0, 8.0)).toList)
        val cgbf1sd = new CGBF(s4g(Vector(0.0, 0.0, 12.0)).toList)
        val cgbf1se = new CGBF(s4g(Vector(0.0, 0.0, 16.0)).toList)

        cgbf1sa.overlap(cgbf1sb) should be (0.189074 plusOrMinus err)
        //cgbf1sa.overlap(cgbf1sc) should be (0.009786 plusOrMinus 0.00001) // data from paper
        cgbf1sa.overlap(cgbf1sc) should be (0.007986 plusOrMinus err) // data from paper but swap 7 and 9
        cgbf1sa.overlap(cgbf1se) should be (0.3E-6   plusOrMinus err)
        cgbf1sa.kinetic(cgbf1sb) should be (-0.002579 plusOrMinus err)
        cgbf1sa.kinetic(cgbf1sc) should be (-0.002310 plusOrMinus err)
        cgbf1sa.nuclear(cgbf1sb, origin) should be (0.091748 plusOrMinus err)
        cgbf1sa.nuclear(cgbf1sc, origin) should be (0.002208 plusOrMinus err)
    }
}
